//  Created by Ed Gamble on 1/31/2018
//  Copyright (c) 2018 breadwallet LLC.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#include "BRCoreJni.h"
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include <BRInt.h>
#include <BRTransaction.h>
#include "com_breadwallet_core_BRCoreTransactionInput.h"

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    createTransactionInput
 * Signature: ([BJJ[B[B[BJ)J
 */
JNIEXPORT jlong JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_createTransactionInput
        (JNIEnv *env, jclass thisClass, jbyteArray hashByteArray, jlong index, jlong amount,
         jbyteArray scriptByteArray,
         jbyteArray signatureByteArray,
         jbyteArray witnessByteArray,
         jlong sequence) {
    BRTxInput *input = (BRTxInput *) calloc(1, sizeof(BRTxInput));

    size_t hashLen = (size_t) (*env)->GetArrayLength(env, hashByteArray);
    const uint8_t *hashData = (const uint8_t *) (*env)->GetByteArrayElements(env, hashByteArray, 0);
    assert (32 == hashLen);

    input->txHash = UInt256Get((const void *) hashData);
    input->index = (uint32_t) index;
    input->amount = (uint32_t) amount;

    // script
    input->script = NULL;
    size_t scriptLen = (size_t) (*env)->GetArrayLength(env, scriptByteArray);
    const uint8_t *script = (const uint8_t *)
            (0 == scriptLen
             ? NULL
             : (*env)->GetByteArrayElements(env, scriptByteArray, 0));
    BRTxInputSetScript(input, script, scriptLen);

    // signature
    input->signature = NULL;
    size_t signatureLen = (size_t) (*env)->GetArrayLength(env, signatureByteArray);
    const uint8_t *signature = (const uint8_t *)
            (0 == signatureLen
             ? NULL
             : (*env)->GetByteArrayElements(env, signatureByteArray, 0));
    BRTxInputSetSignature(input, signature, signatureLen);

    // witness
    input->witness = NULL;
    size_t witnessLen = (size_t) (*env)->GetArrayLength(env, witnessByteArray);
    const uint8_t *witness = (const uint8_t *)
            (0 == witnessLen
             ? NULL
             : (*env)->GetByteArrayElements(env, witnessByteArray, 0));
    BRTxInputSetWitness(input, witness, witnessLen);

    input->sequence = (uint32_t) (sequence == -1 ? TXIN_SEQUENCE : sequence);

    return (jlong) input;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getAddress
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getAddress
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    
    size_t addressLen = sizeof (input->address);
    char address[1 + addressLen];
    memcpy (address, input->address, addressLen);
    address[addressLen] = '\0';

    return (*env)->NewStringUTF (env, address);
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    setAddress
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_setAddress
        (JNIEnv *env, jobject thisObject , jstring addressObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    
    size_t addressLen = sizeof (input->address);

    size_t addressDataLen = (size_t) (*env)->GetStringLength (env, addressObject);
    const jchar *addressData = (*env)->GetStringChars (env, addressObject, 0);
    assert (addressDataLen <= addressLen);

    memset (input->address, '\0', addressLen);
    memcpy (input->address, addressData, addressDataLen);

}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getHash
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getHash
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);

    size_t hashLen = sizeof (UInt256);
    jbyteArray hashByteArray = (*env)->NewByteArray (env, hashLen);
    (*env)->SetByteArrayRegion (env, hashByteArray, 0, (jsize) hashLen, 
                                (const jbyte *) input->txHash.u8);
    
    return hashByteArray;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getIndex
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getIndex
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    return (jlong) input->index;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getAmount
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getAmount
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    return (jlong) input->amount;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getScript
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getScript
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    
    jbyteArray scriptByteArray = (*env)->NewByteArray (env, (jsize) input->scriptLen);
    (*env)->SetByteArrayRegion (env, scriptByteArray, 0, (jsize) input->scriptLen,
                                (const jbyte *) input->script);
    
    return scriptByteArray;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getSignature
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getSignature
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);

    jbyteArray signatureByteArray = (*env)->NewByteArray (env, (jsize) input->sigLen);
    (*env)->SetByteArrayRegion (env, signatureByteArray, 0, (jsize) input->sigLen,
                                (const jbyte *) input->signature);

    return signatureByteArray;
}

/*
 * Class:     com_breadwallet_core_BRCoreTransactionInput
 * Method:    getSequence
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_com_breadwallet_core_BRCoreTransactionInput_getSequence
        (JNIEnv *env, jobject thisObject) {
    BRTxInput *input = (BRTxInput *) getJNIReference (env, thisObject);
    return (jlong) input->sequence;
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getDownloadPeerName(JNIEnv *env, jobject instance) {

    // TODO

    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jint JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getPeerCount(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jdouble JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getSyncProgress(JNIEnv *env, jobject instance,
                                                            jlong startHeight) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_disconnect(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jint JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getConnectStatusValue(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_connect(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_jniUseFixedPeer(JNIEnv *env, jobject instance,
                                                            jstring node_, jint port) {
    const char *node = (*env)->GetStringUTFChars(env, node_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, node_, node);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getCurrentPeerName(JNIEnv *env, jobject instance) {

    // TODO

    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_rescan(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_rescanFromBlock(JNIEnv *env, jobject instance,
                                                            jlong blockNumber) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_rescanFromCheckPoint(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getEstimatedBlockHeight(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getLastBlockHeight(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getLastBlockTimestamp(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getHash(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getVersion(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_createTransactionInput(JNIEnv *env,
                                                                            jclass type,
                                                                            jbyteArray hash_,
                                                                            jlong index,
                                                                            jlong amount,
                                                                            jbyteArray script_,
                                                                            jbyteArray signature_,
                                                                            jbyteArray witness_,
                                                                            jlong sequence) {
    jbyte *hash = (*env)->GetByteArrayElements(env, hash_, NULL);
    jbyte *script = (*env)->GetByteArrayElements(env, script_, NULL);
    jbyte *signature = (*env)->GetByteArrayElements(env, signature_, NULL);
    jbyte *witness = (*env)->GetByteArrayElements(env, witness_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, hash_, hash, 0);
    (*env)->ReleaseByteArrayElements(env, script_, script, 0);
    (*env)->ReleaseByteArrayElements(env, signature_, signature, 0);
    (*env)->ReleaseByteArrayElements(env, witness_, witness, 0);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getAddress(JNIEnv *env, jobject instance) {

    // TODO
    const char *const returnValue = NULL;

    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_setAddress(JNIEnv *env, jobject instance,
                                                                jstring address_) {
    const char *address = (*env)->GetStringUTFChars(env, address_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, address_, address);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getHash(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getIndex(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getAmount(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getScript(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getSignature(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionInput_getSequence(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jobjectArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getInputs(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_createTransactionOutput(JNIEnv *env,
                                                                              jclass type,
                                                                              jlong amount,
                                                                              jbyteArray script_) {
    jbyte *script = (*env)->GetByteArrayElements(env, script_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, script_, script, 0);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_getAddress(JNIEnv *env, jobject instance) {

    // TODO

    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_setAddress(JNIEnv *env, jobject instance,
                                                                 jstring address_) {
    const char *address = (*env)->GetStringUTFChars(env, address_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, address_, address);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_setAmount(JNIEnv *env, jobject instance,
                                                                jlong amount) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_getAmount(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransactionOutput_getScript(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jobjectArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getOutputs(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getLockTime(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_setLockTime(JNIEnv *env, jobject instance,
                                                            jlong lockTime) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getBlockHeight(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getTimestamp(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_setTimestamp(JNIEnv *env, jobject instance,
                                                             jlong timestamp) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_serialize(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_addInput(JNIEnv *env, jobject instance,
                                                         jobject input) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_addOutput(JNIEnv *env, jobject instance,
                                                          jobject output) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_shuffleOutputs(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getSize(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_getStandardFee(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreTransaction_isSigned(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getSecret(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getPubKey(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jint JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getCompressed(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getPrivKey(JNIEnv *env, jobject instance) {

    // TODO

    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getSeedFromPhrase(JNIEnv *env, jclass type,
                                                          jbyteArray phrase_) {
    jbyte *phrase = (*env)->GetByteArrayElements(env, phrase_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, phrase_, phrase, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getAuthPrivKeyForAPI(JNIEnv *env, jclass type,
                                                             jbyteArray seed_) {
    jbyte *seed = (*env)->GetByteArrayElements(env, seed_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, seed_, seed, 0);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_getAuthPublicKeyForAPI(JNIEnv *env, jclass type,
                                                               jbyteArray privKey_) {
    jbyte *privKey = (*env)->GetByteArrayElements(env, privKey_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, privKey_, privKey, 0);
    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_decryptBip38Key(JNIEnv *env, jclass type, jstring privKey_,
                                                        jstring pass_) {
    const char *privKey = (*env)->GetStringUTFChars(env, privKey_, 0);
    const char *pass = (*env)->GetStringUTFChars(env, pass_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, privKey_, privKey);
    (*env)->ReleaseStringUTFChars(env, pass_, pass);
    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_createJniCoreKey(JNIEnv *env, jclass type) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_createCoreKeyForBIP32(JNIEnv *env, jclass type,
                                                              jbyteArray seed_, jlong chain,
                                                              jlong index) {
    jbyte *seed = (*env)->GetByteArrayElements(env, seed_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, seed_, seed, 0);
}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_setPrivKey(JNIEnv *env, jobject instance,
                                                   jstring privKey_) {
    const char *privKey = (*env)->GetStringUTFChars(env, privKey_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, privKey_, privKey);
}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_setSecret(JNIEnv *env, jobject instance, jbyteArray secret_,
                                                  jboolean compressed) {
    jbyte *secret = (*env)->GetByteArrayElements(env, secret_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, secret_, secret, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_compactSign(JNIEnv *env, jobject instance,
                                                    jbyteArray data_) {
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_createKeyRecoverCompactSign(JNIEnv *env, jclass type,
                                                                    jbyteArray data_,
                                                                    jbyteArray signature_) {
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jbyte *signature = (*env)->GetByteArrayElements(env, signature_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseByteArrayElements(env, signature_, signature, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_encryptNative(JNIEnv *env, jobject instance,
                                                      jbyteArray data_, jbyteArray nonce_) {
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jbyte *nonce = (*env)->GetByteArrayElements(env, nonce_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseByteArrayElements(env, nonce_, nonce, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_decryptNative(JNIEnv *env, jobject instance,
                                                      jbyteArray data_, jbyteArray nonce_) {
    jbyte *data = (*env)->GetByteArrayElements(env, data_, NULL);
    jbyte *nonce = (*env)->GetByteArrayElements(env, nonce_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, data_, data, 0);
    (*env)->ReleaseByteArrayElements(env, nonce_, nonce, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_encryptUsingSharedSecret(JNIEnv *env, jobject instance,
                                                                 jbyteArray publicKey_,
                                                                 jbyteArray message_,
                                                                 jbyteArray nonce_) {
    jbyte *publicKey = (*env)->GetByteArrayElements(env, publicKey_, NULL);
    jbyte *message = (*env)->GetByteArrayElements(env, message_, NULL);
    jbyte *nonce = (*env)->GetByteArrayElements(env, nonce_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, publicKey_, publicKey, 0);
    (*env)->ReleaseByteArrayElements(env, message_, message, 0);
    (*env)->ReleaseByteArrayElements(env, nonce_, nonce, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_decryptUsingSharedSecret(JNIEnv *env, jobject instance,
                                                                 jbyteArray publicKey_,
                                                                 jbyteArray bytes_,
                                                                 jbyteArray nonce_) {
    jbyte *publicKey = (*env)->GetByteArrayElements(env, publicKey_, NULL);
    jbyte *bytes = (*env)->GetByteArrayElements(env, bytes_, NULL);
    jbyte *nonce = (*env)->GetByteArrayElements(env, nonce_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, publicKey_, publicKey, 0);
    (*env)->ReleaseByteArrayElements(env, bytes_, bytes, 0);
    (*env)->ReleaseByteArrayElements(env, nonce_, nonce, 0);
}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_createPairingKey(JNIEnv *env, jobject instance,
                                                         jbyteArray identifier_) {
    jbyte *identifier = (*env)->GetByteArrayElements(env, identifier_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, identifier_, identifier, 0);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_address(JNIEnv *env, jobject instance) {

    // TODO


    const char *const returnValue = NULL;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_isValidBitcoinPrivateKey(JNIEnv *env, jclass type,
                                                                 jstring key_) {
    const char *key = (*env)->GetStringUTFChars(env, key_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, key_, key);
}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_isValidBitcoinBIP38Key(JNIEnv *env, jclass type,
                                                               jstring key_) {
    const char *key = (*env)->GetStringUTFChars(env, key_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, key_, key);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_encodeSHA256(JNIEnv *env, jclass type,
                                                     jbyteArray message_) {
    jbyte *message = (*env)->GetByteArrayElements(env, message_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, message_, message, 0);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_encodeSHA256Double(JNIEnv *env, jclass type,
                                                           jbyteArray message_) {
    jbyte *message = (*env)->GetByteArrayElements(env, message_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, message_, message, 0);
}

JNIEXPORT jstring JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_encodeBase58(JNIEnv *env, jclass type,
                                                     jbyteArray message_) {
    jbyte *message = (*env)->GetByteArrayElements(env, message_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, message_, message, 0);

    const char *const returnValue;
    return (*env)->NewStringUTF(env, returnValue);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_decodeBase58(JNIEnv *env, jclass type, jstring message_) {
    const char *message = (*env)->GetStringUTFChars(env, message_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, message_, message);
}

JNIEXPORT jbyteArray JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_sign(JNIEnv *env, jobject instance,
                                             jbyteArray messageDigest_) {
    jbyte *messageDigest = (*env)->GetByteArrayElements(env, messageDigest_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, messageDigest_, messageDigest, 0);
}

JNIEXPORT jboolean JNICALL
Java_com_breadwallet_core_sno_SNCoreKey_verify(JNIEnv *env, jobject instance,
                                               jbyteArray messageDigest_, jbyteArray signature_) {
    jbyte *messageDigest = (*env)->GetByteArrayElements(env, messageDigest_, NULL);
    jbyte *signature = (*env)->GetByteArrayElements(env, signature_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, messageDigest_, messageDigest, 0);
    (*env)->ReleaseByteArrayElements(env, signature_, signature, 0);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_publishTransactionWithListener(JNIEnv *env,
                                                                           jobject instance,
                                                                           jobject transaction,
                                                                           jobject listener) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_getRelayCount(JNIEnv *env, jobject instance,
                                                          jbyteArray txHash_) {
    jbyte *txHash = (*env)->GetByteArrayElements(env, txHash_, NULL);

    // TODO

    (*env)->ReleaseByteArrayElements(env, txHash_, txHash, 0);
}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_testSaveBlocksCallback(JNIEnv *env, jobject instance,
                                                                   jboolean replace,
                                                                   jobjectArray blocks) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_testSavePeersCallback(JNIEnv *env, jobject instance,
                                                                  jboolean replace,
                                                                  jobjectArray peers) {

    // TODO

}

JNIEXPORT jlong JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_createCorePeerManager(JNIEnv *env, jclass type,
                                                                  jobject params, jobject wallet,
                                                                  jdouble earliestKeyTime,
                                                                  jobjectArray blocks,
                                                                  jobjectArray peers) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_installListener(JNIEnv *env, jobject instance,
                                                            jobject listener) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_disposeNative(JNIEnv *env, jobject instance) {

    // TODO

}

JNIEXPORT void JNICALL
Java_com_breadwallet_core_sno_SNPeerManager_initializeNative(JNIEnv *env, jclass type) {

    // TODO

}