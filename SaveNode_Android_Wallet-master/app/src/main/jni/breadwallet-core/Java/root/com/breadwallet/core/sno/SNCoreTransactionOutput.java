package com.breadwallet.core.sno;

public class SNCoreTransactionOutput extends SNJniReference {

    public SNCoreTransactionOutput(long amount,
                                   byte[] script) {
        this(createTransactionOutput(amount, script));
    }

    public SNCoreTransactionOutput(long jniReferenceAddress) {
        super(jniReferenceAddress);
    }

    protected static native long createTransactionOutput(long amount,
                                                         byte[] script);

    public native String getAddress();

    private native void setAddress(String address);

    public native long getAmount();

    /**
     * Change the output amount - typically used after computing transaction fees.
     */
    public native void setAmount(long amount);

    public native byte[] getScript();
}
