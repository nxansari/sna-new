package com.breadwallet.core.sno;

public class SNCoreTransactionInput extends SNJniReference {

    /**
     * Create a transaction input.
     *
     * @param hash
     * @param index
     * @param amount
     * @param script
     * @param signature
     * @param sequence The sequence number.  If -1, TXIN_SEQUENCE will be used.
     */
    public SNCoreTransactionInput (byte[] hash, long index, long amount,
                                   byte[] script,
                                   byte[] signature,
                                   byte[] witness,
                                   long sequence) {
        this (createTransactionInput(hash, index, amount, script, signature, witness, sequence));
    }

    public SNCoreTransactionInput(long jniReferenceAddress) {
        super(jniReferenceAddress);
    }

    protected static native long createTransactionInput (byte[] hash, long index, long amount,
                                                         byte[] script,
                                                         byte[] signature,
                                                         byte[] witness,
                                                         long sequence);

    public native String getAddress ();

    private native void setAddress (String address);

    public native byte[] getHash ();

    public native long getIndex ();

    public native long getAmount ();

    public native byte[] getScript ();

    public native byte[] getSignature ();

    public native long getSequence ();
}
