package com.breadwallet.core.sno;

public class SNCoreChainParams extends SNJniReference {

    private SNCoreChainParams(long jniReferenceAddress) {
        super(jniReferenceAddress);
    }

    //
    public native int getJniMagicNumber();

    //
    //
    //

    public static SNCoreChainParams mainnetChainParams =
            new SNCoreChainParams(createJniMainnetChainParams());

    private static native long createJniMainnetChainParams();

    public static SNCoreChainParams testnetChainParams =
            new SNCoreChainParams(createJniTestnetChainParams());

    private static native long createJniTestnetChainParams();

    public static SNCoreChainParams mainnetBcashChainParams =
            new SNCoreChainParams(createJniMainnetBcashChainParams());

    private static native long createJniMainnetBcashChainParams();

    public static SNCoreChainParams testnetBcashChainParams =
            new SNCoreChainParams(createJniTestnetBcashChainParams());

    private static native long createJniTestnetBcashChainParams();
}
